import os.path
import re
from io import StringIO



class FileReader :
    def __init__(self, path):
        self.path = path
    
    def word_iter(self) -> str:
        
        with open(self.path, 'r') as f:
            
            # output = StringIO()
            # isword = False
            for line in f:
                for word in (x.group(0) for x in re.finditer(r"[A-Za-z0-9-']+", line)) :
                    yield word
                   
                # for char in s:
                #     # print(char)
                #     if char.isalpha() or char.isdigit():
                #         isword = True
                #         output.write(char)
                    
                #     else:
                #         if isword is False:
                #             continue

                #         yield output.getvalue()
                #         output = StringIO()
                #         isword = False
    
    def num_iter(self):
        return ( word for word in self.word_iter() if word.isnumeric() )
    
    def engword_iter(self):
        return (word for word in self.word_iter() if word.isalpha())
