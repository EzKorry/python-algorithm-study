from typing import List, Hashable


class LinearProbingHashSt:
    def __init__(self, cap=16):
        self.N = 0
        self.M = cap
        self.keys: List[Hashable] = [None] * self.M
        self.vals = [None] * self.M

    # generator
    def iter(self, key):
        i = self.hash(key)
        while self.keys[i] is not None:
            yield i
            i = (i + 1) % self.M
        # raise StopIteration

    def hash(self, key):
        return (hash(key) & 0x7fffffff) % self.M

    def resize(self, size):
        temp = LinearProbingHashSt(size)

        gen = ((self.keys[i], self.vals[i])
               for i in range(0, self.M)
               if self.keys[i] is not None)

        for key, val in gen:
            temp.put(key, val)

        self.keys = temp.keys
        self.vals = temp.vals
        self.M = temp.M

    def put(self, key, val):
        if self.N >= self.M / 2:
            self.resize(2 * self.M)

        i = self.hash(key)
        while self.keys[i] is not None:
            if self.keys[i] == key:
                self.vals[i] = val
                return
            i = (i + 1) % self.M
        self.keys[i] = key
        self.vals[i] = val
        self.N += 1

    def get(self, key):
        for i in self.iter(key):
            if self.keys[i] == key:
                return self.vals[i]
        return None

    def to_str(self):
        s = ', '.join([f'{key}:{self.vals[i]}' if key is not None else '-' for i, key in enumerate(self.keys)])
        return f'{{{s}}}'
