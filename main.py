from linear_probing_hash_ST import LinearProbingHashSt
from rapid_quick3way import RapidQuick3Way, exch
from file_reader import FileReader
from random import randint
import time




def linearProbingHastStTest():
    m = LinearProbingHashSt()
    m.put(1, 4)
    m.put(2, 5)
    m.put(3, 4)
    for i in range(2000):
        key, value = randint(1, 7), i
        # print("put:", key, value)
        m.put(key, value)
    print(m.to_str())

def RapidQuick3WayTest() :
    m = FileReader("algs4-data/tale.txt")

    ls = list(m.word_iter())
    ms = time.time()
    r = RapidQuick3Way(ls)
    ms = time.time() - ms
    # print(' '.join(r), ms)

    ls = list(m.word_iter())
    ms2 = time.time()
    ls.sort()
    ms2 = time.time() - ms2
    # print(' '.join(r), ms)
    print(ms, ms2)


def fileReaderTest():
    m = FileReader("algs4-data/leipzig1M.txt")
    for word in m.word_iter():
        print(word, end=' ')
        # print(word)
    print("word_iter end")
    for word in m.engword_iter():
        print(word, end=" ")
    print("engword_iter end")
    for word in m.num_iter():
        print(word, end=" ")
    print("num_iter end")

    
        


if __name__ == '__main__':
    # linearProbingHastStTest()
    # fileReaderTest()
    RapidQuick3WayTest()