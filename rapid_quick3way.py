from typing import List, Any

def exch(vec, a, b) :
    vec[a], vec[b] = vec[b], vec[a]

def less(vec: List[Any]):
    pass

def show(vec: List[Any]):
    pass

def isSorted(vec: List[Any]) -> bool:
    pass

class RapidQuick3Way :
    def __init__(self, vec: List[Any]) :
        self.a = vec
        self._sort(0, len(self.a) - 1)

    def _sort(self, lo, hi):
        if lo >= hi : return

        p = lo
        i = lo - 1
        q = hi
        j = hi + 1
        a = self.a

        while True :
            while True:
                i += 1
                if i > hi: break
                if a[i] > a[lo] : break
                elif a[i] == a[lo] and i < j :
                    exch(a, i, p)
                    p += 1
            while True:
                j -= 1
                if j < lo: break
                if a[lo] > a[j] : break
                elif a[lo] == a[j] and i < j :
                    exch(a, j, q)
                    q -= 1
            if i >= j : break
            exch(a, i, j)

        left_same_count = p - lo
        right_same_count = hi - q
        leftmove = min(j- p + 1, left_same_count)
        rightmove = min(q - i + 1, right_same_count)

        mj = j
        mi = i

        while leftmove > 0 and mj >= lo : 
            leftmove -= 1
            exch(a, lo + leftmove, mj)
            mj -= 1
        while rightmove > 0 and mi <= hi :
            rightmove -= 1
            exch(a, hi - rightmove, mi)
            mi += 1

        j -= left_same_count
        i += right_same_count
        
        self._sort(lo, j)
        self._sort(i, hi)

    def __iter__(self):
        return iter(self.a)




